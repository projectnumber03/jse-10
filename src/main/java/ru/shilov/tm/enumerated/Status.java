package ru.shilov.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Status {

    PLANNED("Запланировано"),
    IN_PROCESS("В процессе"),
    DONE("Готово");

    private final String description;

}
