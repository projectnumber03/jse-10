package ru.shilov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.service.ITerminalService;
import ru.shilov.tm.entity.AbstractEntity;

import java.util.List;
import java.util.Scanner;

public final class TerminalServiceImpl implements ITerminalService {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);

    @NotNull
    @Override
    public String nextLine() {
        return scanner.nextLine();
    }

    @Override
    public void printAll(@NotNull final List<? extends AbstractEntity> entities) {
        for (int i = 1; i <= entities.size(); i++) {
            System.out.println(String.format("%d. %s", i, entities.get(i - 1).getName()));
        }
    }

}
