package ru.shilov.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.*;
import ru.shilov.tm.api.repository.IUserRepository;

import java.util.Optional;

@AllArgsConstructor
public final class UserServiceImpl extends AbstractService<User> implements IUserService {

    @Getter
    private final IUserRepository repository;

    @NotNull
    @Override
    public Boolean removeOneByUserId(@Nullable final String id, @Nullable final String userId) throws EntityRemoveException {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) throw new EntityRemoveException();
        return repository.removeOneByUserId(id, userId);
    }

    @Nullable
    @Override
    public User persist(@Nullable final User user) throws EntityPersistException {
        if (user == null) throw new EntityPersistException();
        return repository.persist(user);
    }

    @NotNull
    @Override
    public String getId(@Nullable final String value) throws NumberToIdTransformException {
        if (!isNumber(value)) throw new NumberToIdTransformException(value);
        return repository.getId(value);
    }

    @NotNull
    @Override
    public Boolean containsLogin(@Nullable final String login) throws NoSuchEntityException {
        if (login == null || login.isEmpty()) throw new NoSuchEntityException();
        return repository.containsLogin(login);
    }

    @NotNull
    @Override
    public Optional<User> findByLoginPassword(@Nullable final String login, @Nullable final String password) throws NoSuchEntityException {
        if (login == null || login.isEmpty() || password == null || password.isEmpty()) throw new NoSuchEntityException();
        return repository.findByLoginPassword(login, password);
    }

    private boolean isNumber(@Nullable final String value) {
        return value != null && !value.isEmpty() && value.matches("\\d+");
    }

}
