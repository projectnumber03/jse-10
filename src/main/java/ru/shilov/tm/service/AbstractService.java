package ru.shilov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.service.IService;
import ru.shilov.tm.entity.AbstractEntity;
import ru.shilov.tm.error.EntityMergeException;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.api.repository.IRepository;

import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    @Override
    public List<T> findAll() {
        return getRepository().findAll();
    }

    @NotNull
    @Override
    public T findOne(@Nullable final String id) throws NoSuchEntityException {
        @Nullable
        final T entity;
        if (id == null || id.isEmpty() || (entity = getRepository().findOne(id)) == null) {
            throw new NoSuchEntityException();
        }
        return entity;
    }

    @Override
    public void removeAll() {
        getRepository().removeAll();
    }

    @Nullable
    @Override
    public T persist(@Nullable final T entity) throws EntityPersistException {
        if (entity == null) throw new EntityPersistException();
        return getRepository().persist(entity);
    }

    @Nullable
    @Override
    public T merge(@Nullable final T entity) throws EntityMergeException {
        if (entity == null) throw new EntityMergeException();
        return getRepository().merge(entity);
    }

    @NotNull
    abstract IRepository<T> getRepository();

}
