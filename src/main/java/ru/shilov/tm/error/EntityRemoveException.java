package ru.shilov.tm.error;

public final class EntityRemoveException extends Exception {

    public EntityRemoveException() {
        super("Ошибка удаления объекта");
    }

}
