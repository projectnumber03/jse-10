package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected final Map<String, T> entities = new LinkedHashMap<>();

    @NotNull
    @Override
    public List<T> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Nullable
    @Override
    public T findOne(@NotNull final String id) {
        return entities.values().stream().filter(entity -> entity.getId().equals(id)).findAny().orElse(null);
    }

    @NotNull
    @Override
    public List<T> findByUserId(@NotNull final String userId) {
        return entities.values().stream().filter(entity -> entity.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @NotNull
    @Override
    public Boolean removeByUserId(@NotNull final String userId) {
        return entities.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(userId));
    }

    @NotNull
    @Override
    public Boolean removeOneByUserId(@NotNull final String id, @NotNull final String userId) {
        return entities.entrySet().removeIf(entry -> entry.getValue().getId().equals(id) && entry.getValue().getUserId().equals(userId));
    }

    @Nullable
    @Override
    public T persist(@NotNull final T entity) {
        return entities.putIfAbsent(entity.getId(), entity);
    }

    @NotNull
    @Override
    public T merge(@NotNull final T entity) {
        return entities.merge(entity.getId(), entity, (oldEntity, newEntity) -> newEntity);
    }

}
