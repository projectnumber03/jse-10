package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.entity.Project;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class ProjectRepositoryImpl extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public String getId(@NotNull final String value, @NotNull final String userId) {
        @NotNull
        final List<Project> projects = findByUserId(userId);
        return projects.size() >= Integer.parseInt(value) ? projects.get(Integer.parseInt(value) - 1).getId() : "";
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDescription(@NotNull final String value, @NotNull final String userId) {
        return findByUserId(userId).stream()
                .filter(p -> Objects.requireNonNull(p.getName()).contains(value) || Objects.requireNonNull(p.getDescription()).contains(value))
                .collect(Collectors.toList());
    }

}
