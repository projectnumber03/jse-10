package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    String getId(@NotNull final String value, @NotNull final String userId);

    @NotNull
    List<Project> findByNameOrDescription(@NotNull final String value, @NotNull final String userId);

}
