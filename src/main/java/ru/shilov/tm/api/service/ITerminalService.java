package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.entity.AbstractEntity;

import java.util.List;

public interface ITerminalService {

    @NotNull
    String nextLine();

    void printAll(@NotNull final List<? extends AbstractEntity> entities);

}
