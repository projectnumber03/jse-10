package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.NoSuchEntityException;

import java.util.Arrays;
import java.util.List;

public final class TaskFindAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws NoSuchEntityException {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String userId = getServiceLocator().getAuthorizationService().getCurrentUserId();
        @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findByUserId(userId);
        getServiceLocator().getTerminalService().printAll(tasks);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список задач";
    }

}
