package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public class TaskFindByNameOrDescriptionCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String userId = getServiceLocator().getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ФРАЗУ ДЛЯ ПОИСКА:");
        @NotNull final String searchPhrase = getServiceLocator().getTerminalService().nextLine();
        @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findByNameOrDescription(searchPhrase, userId);
        getServiceLocator().getTerminalService().printAll(tasks);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-search";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск задач";
    }

}
