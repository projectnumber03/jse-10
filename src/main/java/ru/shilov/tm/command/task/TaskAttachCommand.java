package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class TaskAttachCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String userId = getServiceLocator().getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        @NotNull final String taskId = getServiceLocator().getTaskService().getId(getServiceLocator().getTerminalService().nextLine(), userId);
        @NotNull final Task t = getServiceLocator().getTaskService().findOne(taskId);
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        t.setProjectId(getServiceLocator().getProjectService().getId(getServiceLocator().getTerminalService().nextLine(), userId));
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-attach";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Добавление задачи в проект";
    }

}
