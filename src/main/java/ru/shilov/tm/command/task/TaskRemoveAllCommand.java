package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class TaskRemoveAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        getServiceLocator().getTaskService().removeAll();
        System.out.println("[ВСЕ ЗАДАЧИ УДАЛЕНЫ]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove-all";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Полное удаление всех задач";
    }

}
