package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskFindAllSortedByFinishCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String userId = getServiceLocator().getAuthorizationService().getCurrentUserId();
        @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findByUserId(userId).stream()
                .filter(t -> t.getFinish() != null)
                .sorted(Comparator.comparing(Task::getFinish))
                .collect(Collectors.toList());
        getServiceLocator().getTerminalService().printAll(tasks);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list-finish";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список задач по дате окончания";
    }

}
