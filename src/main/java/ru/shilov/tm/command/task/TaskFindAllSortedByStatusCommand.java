package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskFindAllSortedByStatusCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        final String userId = getServiceLocator().getAuthorizationService().getCurrentUserId();
        @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findByUserId(userId).stream()
                .sorted(Comparator.comparing(Task::getStatus))
                .collect(Collectors.toList());
        getServiceLocator().getTerminalService().printAll(tasks);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список задач по статусу готовности";
    }

}
