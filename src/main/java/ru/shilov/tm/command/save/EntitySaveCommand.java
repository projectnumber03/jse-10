package ru.shilov.tm.command.save;

import org.jetbrains.annotations.NotNull;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public final class EntitySaveCommand extends AbstractSaveTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        try (@NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("./saved/entities.bin"))) {
            objectOutputStream.writeObject(getDomain());
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "save";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранение предметной области с использованием сериализации";
    }

}
