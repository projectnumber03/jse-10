package ru.shilov.tm.command.save;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.domain.Domain;

import javax.xml.bind.Marshaller;
import java.io.File;

public final class EntityJaxBXmlSaveCommand extends AbstractSaveTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Marshaller jaxbMarshaller = JAXBContextFactory.createContext(new Class[]{Domain.class}, null).createMarshaller();
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/xml");
        jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(getDomain(), new File("./saved/entities.xml"));
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "save-xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранение предметной области в xml с использованием JAXB";
    }

}
