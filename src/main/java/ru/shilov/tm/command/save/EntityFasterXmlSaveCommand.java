package ru.shilov.tm.command.save;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class EntityFasterXmlSaveCommand extends AbstractSaveTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        new XmlMapper().writerWithDefaultPrettyPrinter().writeValue(new File("./saved/entities.xml"), getDomain());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "save-xml-f";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранение предметной области в xml с использованием FasterXml";
    }

}
