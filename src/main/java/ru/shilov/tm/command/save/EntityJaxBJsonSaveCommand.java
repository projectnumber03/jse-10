package ru.shilov.tm.command.save;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.domain.Domain;

import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.Collections;

public final class EntityJaxBJsonSaveCommand extends AbstractSaveTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Marshaller jaxbMarshaller = JAXBContextFactory.createContext(new Class[]{Domain.class}, Collections.emptyMap()).createMarshaller();
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(getDomain(), new File("./saved/entities.json"));
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "save-json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранение предметной области в json с использованием JAXB";
    }

}
