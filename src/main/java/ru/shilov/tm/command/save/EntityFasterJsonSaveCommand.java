package ru.shilov.tm.command.save;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class EntityFasterJsonSaveCommand extends AbstractSaveTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(new File("./saved/entities.json"), getDomain());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "save-json-f";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранение предметной области в json с использованием FasterXml";
    }

}
