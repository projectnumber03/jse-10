package ru.shilov.tm.command.save;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.domain.Domain;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public abstract class AbstractSaveTerminalCommand extends AbstractTerminalCommand {

    protected Domain getDomain() {
        @NotNull final List<Project> projects = getServiceLocator().getProjectService().findAll();
        @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAll();
        @NotNull final List<User> users = getServiceLocator().getUserService().findAll();
        return new Domain(projects, tasks, users);
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

}
