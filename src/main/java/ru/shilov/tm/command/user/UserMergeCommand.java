package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class UserMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        @NotNull final String userId = getServiceLocator().getUserService().getId(getServiceLocator().getTerminalService().nextLine());
        @NotNull final User u = getServiceLocator().getUserService().findOne(userId);
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        u.setLogin(getServiceLocator().getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(getServiceLocator().getTerminalService().nextLine());
        Arrays.asList(User.Role.values()).forEach(r -> System.out.println(String.format("%d. %s", r.ordinal() + 1, r.getDescription())));
        System.out.println("ВЫБЕРИТЕ РОЛЬ:");
        @NotNull String roleId = getServiceLocator().getTerminalService().nextLine();
        while (!roleCheck(roleId)) {
            System.out.println("ВЫБЕРИТЕ РОЛЬ:");
            roleId = getServiceLocator().getTerminalService().nextLine();
        }
        u.setRole(User.Role.values()[Integer.parseInt(roleId) - 1]);
        getServiceLocator().getUserService().merge(u);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Редактирование пользователя";
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

    @NotNull
    private Boolean roleCheck(@Nullable final String roleId) {
        return roleId != null && !roleId.isEmpty()
                && roleId.matches("\\d+")
                && Integer.parseInt(roleId) <= User.Role.values().length;
    }

}
