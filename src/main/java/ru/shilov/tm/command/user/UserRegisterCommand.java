package ru.shilov.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Collections;
import java.util.List;

public final class UserRegisterCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final User u = new User();
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        u.setLogin(getServiceLocator().getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(DigestUtils.md5Hex(getServiceLocator().getTerminalService().nextLine()));
        u.setRole(User.Role.USER);
        getServiceLocator().getUserService().persist(u);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public String getName() {
        return "register";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Регистрация пользователя";
    }

}
