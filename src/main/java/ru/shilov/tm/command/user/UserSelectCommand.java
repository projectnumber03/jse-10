package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class UserSelectCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        @NotNull final String userId = getServiceLocator().getUserService().getId(getServiceLocator().getTerminalService().nextLine());
        @NotNull final User u = getServiceLocator().getUserService().findOne(userId);
        System.out.println(u.toString());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-select";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Свойства пользователя";
    }

}
