package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class UserFindAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final List<User> users = getServiceLocator().getUserService().findAll();
        getServiceLocator().getTerminalService().printAll(users);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список пользователей";
    }

}
