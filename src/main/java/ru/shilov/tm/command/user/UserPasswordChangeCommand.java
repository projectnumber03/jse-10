package ru.shilov.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class UserPasswordChangeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final User u = getServiceLocator().getUserService().findOne(getServiceLocator().getAuthorizationService().getCurrentUserId());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(DigestUtils.md5Hex(getServiceLocator().getTerminalService().nextLine()));
        getServiceLocator().getUserService().merge(u);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "password-change";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение пароля";
    }

}
