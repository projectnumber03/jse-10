package ru.shilov.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.NoSuchEntityException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public final class UserLoginCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws NoSuchEntityException {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        @NotNull String login = getServiceLocator().getTerminalService().nextLine();
        while (!getServiceLocator().getUserService().containsLogin(login)) {
            System.out.println("[ПОЛЬЗОВАТЕЛЬ НЕ НАЙДЕН]");
            System.out.println("ВВЕДИТЕ ЛОГИН:");
            login = getServiceLocator().getTerminalService().nextLine();
        }
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        @NotNull String password = getServiceLocator().getTerminalService().nextLine();
        @NotNull Optional<User> user;
        while (!(user = getServiceLocator().getUserService().findByLoginPassword(login, DigestUtils.md5Hex(password))).isPresent()) {
            System.out.println("[НЕВЕРНЫЙ ПАРОЛЬ]");
            System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
            password = getServiceLocator().getTerminalService().nextLine();
        }
        getServiceLocator().getAuthorizationService().setCurrentUser(user);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public String getName() {
        return "login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Авторизация пользователя";
    }

}
