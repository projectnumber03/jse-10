package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectFindAllSortedByStartCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String userId = getServiceLocator().getAuthorizationService().getCurrentUserId();
        @NotNull final List<Project> projects = getServiceLocator().getProjectService().findByUserId(userId).stream()
                .filter(p -> p.getStart() != null)
                .sorted(Comparator.comparing(Project::getStart))
                .collect(Collectors.toList());
        getServiceLocator().getTerminalService().printAll(projects);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list-start";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список проектов по дате начала";
    }

}
