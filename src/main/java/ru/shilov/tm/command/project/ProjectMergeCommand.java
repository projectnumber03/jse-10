package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.enumerated.Status;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;

public final class ProjectMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String userId = getServiceLocator().getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectId = getServiceLocator().getProjectService().getId(getServiceLocator().getTerminalService().nextLine(), userId);
        @NotNull final Project p = getServiceLocator().getProjectService().findOne(projectId);
        System.out.println("ВВЕДИТЕ НАЗВАНИЕ ПРОЕКТА:");
        p.setName(getServiceLocator().getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ОПИСАНИЕ:");
        p.setDescription(getServiceLocator().getTerminalService().nextLine());
        try {
            System.out.println("ВВЕДИТЕ ДАТУ НАЧАЛА:");
            p.setStart(LocalDate.parse(getServiceLocator().getTerminalService().nextLine(), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
            System.out.println("ВВЕДИТЕ ДАТУ ОКОНЧАНИЯ:");
            p.setFinish(LocalDate.parse(getServiceLocator().getTerminalService().nextLine(), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        } catch (DateTimeParseException e) {
            throw new ru.shilov.tm.error.DateTimeParseException();
        }
        Arrays.asList(Status.values()).forEach(r -> System.out.println(String.format("%d. %s", r.ordinal() + 1, r.getDescription())));
        System.out.println("ВЫБЕРИТЕ СТАТУС:");
        @NotNull
        String statusId = getServiceLocator().getTerminalService().nextLine();
        while (!statusCheck(statusId)) {
            System.out.println("ВЫБЕРИТЕ СТАТУС:");
            statusId = getServiceLocator().getTerminalService().nextLine();
        }
        p.setStatus(Status.values()[Integer.parseInt(statusId) - 1]);
        getServiceLocator().getProjectService().merge(p);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Редактирование проекта";
    }

    @NotNull
    private Boolean statusCheck(@Nullable final String statusId) {
        return statusId != null && !statusId.isEmpty()
                && statusId.matches("\\d+")
                && Integer.parseInt(statusId) <= Status.values().length;
    }

}
