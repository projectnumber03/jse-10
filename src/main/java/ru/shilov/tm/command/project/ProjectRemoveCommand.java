package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class ProjectRemoveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String userId = getServiceLocator().getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectId = getServiceLocator().getProjectService().getId(getServiceLocator().getTerminalService().nextLine(), userId);
        getServiceLocator().getProjectService().removeOneByUserId(projectId, userId);
        System.out.println("[ПРОЕКТ УДАЛЕН]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление проекта";
    }

}
