package ru.shilov.tm.command.load;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.domain.Domain;

import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class EntityJaxBJsonLoadCommand extends AbstractLoadTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        removeAll();
        @NotNull final Unmarshaller unmarshaller = JAXBContextFactory.createContext(new Class[]{Domain.class}, null).createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(new File("./saved/entities.json"));
        persistAll(domain);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "load-json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузка предметной области из json с использованием JAXB";
    }

}
