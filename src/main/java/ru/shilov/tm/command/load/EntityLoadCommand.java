package ru.shilov.tm.command.load;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.domain.Domain;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class EntityLoadCommand extends AbstractLoadTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        removeAll();
        try (@NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("./saved/entities.bin"))) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            persistAll(domain);
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузка предметной области с использованием сериализации";
    }


}
