package ru.shilov.tm.command.load;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.domain.Domain;

import java.io.File;

public final class EntityFasterJsonLoadCommand extends AbstractLoadTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        removeAll();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
        @NotNull final Domain domain = objectMapper.readValue(new File("./saved/entities.json"), Domain.class);
        persistAll(domain);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "load-json-f";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузка предметной области из json с использованием FasterXml";
    }

}
