package ru.shilov.tm.command.load;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.domain.Domain;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityPersistException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public abstract class AbstractLoadTerminalCommand extends AbstractTerminalCommand {

    protected void removeAll() {
        getServiceLocator().getAuthorizationService().setCurrentUser(Optional.empty());
        getServiceLocator().getProjectService().removeAll();
        getServiceLocator().getTaskService().removeAll();
        getServiceLocator().getUserService().removeAll();
    }

    protected void persistAll(@NotNull final Domain domain) throws EntityPersistException {
        for (@NotNull final Project p : domain.getProjects()) {
            getServiceLocator().getProjectService().persist(p);
        }

        for (@NotNull final Task t : domain.getTasks()) {
            getServiceLocator().getTaskService().persist(t);
        }

        for (@NotNull final User u : domain.getUsers()) {
            getServiceLocator().getUserService().persist(u);
        }
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

}
