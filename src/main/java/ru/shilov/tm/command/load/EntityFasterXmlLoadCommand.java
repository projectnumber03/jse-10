package ru.shilov.tm.command.load;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.domain.Domain;

import java.io.File;

public final class EntityFasterXmlLoadCommand extends AbstractLoadTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        removeAll();
        @NotNull final Domain domain = new XmlMapper().readValue(new File("./saved/entities.xml"), Domain.class);
        persistAll(domain);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "load-xml-f";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузка предметной области из xml с использованием FasterXml";
    }

}
