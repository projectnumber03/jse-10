package ru.shilov.tm.command.other;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Collections;
import java.util.List;

public final class AboutCommand extends AbstractTerminalCommand {

    @Override
    public void execute() {
        System.out.println("Разработчик: " + Manifests.read("developer"));
        System.out.println("Email: " + Manifests.read("email"));
        System.out.println("Организация: " + Manifests.read("organization"));
        System.out.println(Manifests.read("organizationUrl"));
        System.out.println("Сборка: " + Manifests.read("buildNumber"));
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "О программе";
    }

}
