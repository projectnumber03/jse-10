package ru.shilov.tm.command.other;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.*;
import java.util.stream.Collectors;

public final class HelpCommand extends AbstractTerminalCommand {

    @Setter
    @Nullable
    private Map<String, AbstractTerminalCommand> commands;

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список доступных комманд";
    }

    @Override
    public void execute() {
        @NotNull
        final String helpMessage = Objects.requireNonNull(commands).values().stream()
                .sorted(Comparator.comparing(AbstractTerminalCommand::getName))
                .map(tc -> String.format("%s: %s", tc.getName(), tc.getDescription()))
                .collect(Collectors.joining("\n"));

        System.out.println(helpMessage);
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Collections.emptyList();
    }

}
