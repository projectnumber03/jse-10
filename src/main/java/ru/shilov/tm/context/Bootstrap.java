package ru.shilov.tm.context;

import lombok.Getter;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.shilov.tm.api.context.ServiceLocator;
import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.api.repository.IUserRepository;
import ru.shilov.tm.api.service.*;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.command.other.HelpCommand;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.IllegalCommandException;
import ru.shilov.tm.error.InitializationException;
import ru.shilov.tm.error.PermissionException;
import ru.shilov.tm.repository.ProjectRepositoryImpl;
import ru.shilov.tm.repository.TaskRepositoryImpl;
import ru.shilov.tm.repository.UserRepositoryImpl;
import ru.shilov.tm.service.*;

import java.io.File;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IProjectRepository projectRepo = new ProjectRepositoryImpl();

    @NotNull
    private final ITaskRepository taskRepo = new TaskRepositoryImpl();

    @NotNull
    private final IUserRepository userRepo = new UserRepositoryImpl();

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalServiceImpl();

    @Getter
    @NotNull
    private final IAuthorizationService authorizationService = new AuthorizationServiceImpl();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectServiceImpl(projectRepo, taskRepo);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskServiceImpl(taskRepo);

    @Getter
    @NotNull
    private final IUserService userService = new UserServiceImpl(userRepo);

    public void init() throws Exception {
        System.setProperty("javax.xml.bind.JAXBContextFactory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        new File("./saved").mkdirs();
        initUsers();
        @NotNull final Map<String, AbstractTerminalCommand> commands = initCommands();
        System.out.println("*** ДОБРО ПОЖАЛОВАТЬ В ПЛАНИРОВЩИК ЗАДАЧ ***");
        while (true) {
            try {
                @NotNull final String commandName = terminalService.nextLine();
                if (!commands.containsKey(commandName)) throw new IllegalCommandException();
                @NotNull final AbstractTerminalCommand command = commands.get(commandName);
                if (!command.getRoles().isEmpty() && !authorizationService.hasAnyRole(command.getRoles())) {
                    throw new PermissionException();
                }
                command.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, AbstractTerminalCommand> initCommands() {
        @NotNull final Map<String, AbstractTerminalCommand> commands = new HashMap<>();
        @NotNull final String prefix = Arrays.stream(Package.getPackages())
                .map(Package::getName)
                .filter(name -> name.endsWith("command"))
                .findAny().orElse("");
        commands.putAll(new Reflections(prefix).getSubTypesOf(AbstractTerminalCommand.class).stream()
                .filter(clazz -> !Modifier.isAbstract(clazz.getModifiers())).map(clazz -> {
                    try {
                        @NotNull final AbstractTerminalCommand command = clazz.getDeclaredConstructor().newInstance();
                        command.setServiceLocator(this);
                        if (command instanceof HelpCommand) ((HelpCommand) command).setCommands(commands);
                        return command;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }).filter(Objects::nonNull).collect(Collectors.toMap(AbstractTerminalCommand::getName, command -> command)));
        return commands;
    }

    private void initUsers() throws Exception {
        try {
            userService.persist(new User("user", DigestUtils.md5Hex("123"), User.Role.USER));
            userService.persist(new User("admin", DigestUtils.md5Hex("123"), User.Role.ADMIN));
        } catch (EntityPersistException e) {
            throw new InitializationException();
        }
    }

}
